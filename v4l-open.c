/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (c) 2009 Filippo Argiolas <filippo.argiolas@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details:
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/videodev2.h>


int main(int argc, char *argv[]) {
        int fd = -1;
        char *device;
	struct v4l2_ext_controls v;
        int c;

        device = argv[1];
        if (!device) {
		printf("usage: %s <device node>\n", argv[0]);
                return 2;
	}
        fd = open(device, O_RDONLY);
        if (fd < 0)
                return 3;

        ioctl(fd, VIDIOC_G_EXT_CTRLS, &v);

//	system("echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind");
//	ioctl(fd, VIDIOC_G_EXT_CTRLS, &v);
        return 0;
}
